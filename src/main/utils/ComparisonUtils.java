package main.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.im4java.core.CompareCmd;
import org.im4java.core.IM4JavaException;
import org.im4java.core.IMOperation;
import org.im4java.core.Stream2BufferedImage;
import org.im4java.process.ProcessStarter;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

// Rauno did this
// TODO: Just write your own comparator instead of using ImageMagick

public class ComparisonUtils{
	
	/**
	 * Convenience method - allows the ImageMagick install path (eg. "C:\Program Files\ImageMagick-6.9.3-Q16") to be globally set for application, 
	 * thus allowing other ComparisonUtils methods to be called without the ImageMagick install path as a parameter.
	 * This method should be run once when a testsuite is started or when the test application starts.
	 * This method saves state and is required to run methods which have this method marked as "see also".
 	 * NOTE: Suggest using Java Path instead of String or resolving a Java Path object to String - this is least error-prone.
 	 *  
	 * @param path	an absolute path to the folder ImageMagick is installed in
	 */
	public static void setImageMagickInstallPath(String path){
		ProcessStarter.setGlobalSearchPath(path);
	}
	
	/**
	 * Convenience method - allows the ImageMagick install path (eg. "C:\Program Files\ImageMagick-6.9.3-Q16") to be globally set for application, 
	 * thus allowing other ComparisonUtils methods to be called without the ImageMagick install path as a parameter.
	 * This method should be run once when a testsuite is started or when the test application starts.
	 * This method saves state and is required to run methods which have this method marked as "see also".
 	 *  
	 * @param path	an absolute path to the folder ImageMagick is installed in
	 */
	public static void setImageMagickInstallPath(Path path){
		ProcessStarter.setGlobalSearchPath(path.toString());
	}
	
	/**
 	 * Convenience method - opens the initial screenshot file as a BufferedImage
 	 * File must be in .png format.
 	 * This method does not save any state and thus is not required to use any other method in this class.
 	 * But it makes life a bit easier when coding.
 	 * NOTE: Suggest using Java Path instead of String or resolving a Java Path object to String - this is least error-prone.
 	 * 
 	 * @param targetFileName	The target filename and path - must be .png
 	 * @throws IOException
 	 */
	public static BufferedImage getInitialScreenshot(String targetFileName) throws IOException{
		File savedScreenShot = new File(targetFileName);
		return ImageIO.read(savedScreenShot);
	}
	
	/**
 	 * Convenience method - opens the initial screenshot file as a BufferedImage
 	 * File must be in .png format.
 	 * This method does not save any state and thus is not required to use any other method in this class.
 	 * But it makes life a bit easier when coding.
 	 * 
 	 * @param targetFileName	The target filename and path - must be .png
 	 * @throws IOException
 	 */
	public static BufferedImage getInitialScreenshot(Path targetFileName) throws IOException{
		File savedScreenShot = new File(targetFileName.toString());
		return ImageIO.read(savedScreenShot);
	}
	
	
 	/**
 	 * Convenience method - takes a screenshot using Selenium and saves it to the target path.
 	 * File is saved in .png format.
 	 * This method does not save any state and thus is not required to use any other method in this class.
 	 * But it makes life a bit easier when coding.
 	 * NOTE: Suggest using Java Path instead of String (for targetFileName) or resolving a Java Path object to String - this is least error-prone.
 	 *  
 	 * @param targetFileName	The target filename and path
 	 * @param driver			The webdriver which will take the screenshot
 	 * @throws Exception
 	 */
 	public static void saveInitialScreenshot(String targetFileName, WebDriver driver) throws Exception{
 		File currentScreen = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
 		File targetFile = new File(targetFileName);
 		FileUtils.copyFile(currentScreen, targetFile);
 	}
 	
 	/**
 	 * Convenience method - takes a screenshot using Selenium and saves it to the target path.
 	 * File is saved in .png format.
 	 * This method does not save any state and thus is not required to use any other method in this class.
 	 * But it makes life a bit easier when coding.
 	 *  
 	 * @param targetFileName	The target filename and path
 	 * @param driver			The webdriver which will take the screenshot
 	 * @throws Exception
 	 */
 	public static void saveInitialScreenshot(Path targetFileName, WebDriver driver) throws Exception{
 		File currentScreen = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
 		File targetFile = new File(targetFileName.toString());
 		FileUtils.copyFile(currentScreen, targetFile);
 	}
 	
 	/**
 	 * This method will compare two BufferedImages and produces comparison image (different pixels in red)
 	 * and will count how many pixels are different. The comparison is presented as a container object.
 	 * During this operation, ImageMagick tries to compare .png files even though input & output is BufferedImage.
 	 * All files should be .png format.
 	 * NOTE: Suggest using Java Path instead of String (for imageMagickInstallPath) or resolving a Java Path object to String - this is least error-prone.
 	 * 
 	 * @param initialScreen 			Image to compare to
 	 * @param currentScreen				Image that will be compared
 	 * @param imageMagickInstallPath	Path to folder where ImageMagick is installed
 	 * @return							An element which contains the data of the comparison
 	 * @throws IOException
 	 * @throws InterruptedException
 	 */
 	public static ComparisonResult getComparisonResult(BufferedImage initialScreen, BufferedImage currentScreen, String imageMagickInstallPath) throws IOException, InterruptedException{
 		setImageMagickInstallPath(imageMagickInstallPath);
 		return getComparisonResult(initialScreen, currentScreen);
 	}
 	
 	/**
 	 * This method will compare two BufferedImages and produces comparison image (different pixels in red)
 	 * and will count how many pixels are different. The comparison is presented as a container object.
 	 * During this operation, ImageMagick tries to compare .png files even though input & output is BufferedImage.
 	 * All files should be .png format.
 	 * 
 	 * @param initialScreen 			Image to compare to
 	 * @param currentScreen				Image that will be compared
 	 * @param imageMagickInstallPath	Path to folder where ImageMagick is installed
 	 * @return							An element which contains the data of the comparison
 	 * @throws IOException
 	 * @throws InterruptedException
 	 */
 	public static ComparisonResult getComparisonResult(BufferedImage initialScreen, BufferedImage currentScreen, Path imageMagickInstallPath) throws IOException, InterruptedException{
 		setImageMagickInstallPath(imageMagickInstallPath);
 		return getComparisonResult(initialScreen, currentScreen);
 	}
 	
 	/**
 	 * This method will compare two BufferedImages and produces comparison image (different pixels in red)
 	 * and will count how many pixels are different. The comparison is presented as a container object.
 	 * This method requires the ImageMagick install path to be set before it is run.
 	 * During this operation, ImageMagick tries to compare .png files even though input & output is BufferedImage.
 	 * All files should be .png format.
 	 * 
 	 * @param initialScreen Image to compare to
 	 * @param currentScreen	Image that will be compared
 	 * @return				An element which contains the data of the comparison
 	 * @throws IOException
 	 * @throws InterruptedException
 	 * @see #setImageMagicInstallPath(String)
 	 */
 	public static ComparisonResult getComparisonResult(BufferedImage initialScreen, BufferedImage currentScreen) throws IOException, InterruptedException{
 		
 		ComparisonResult comparisonResult = new ComparisonResult();
 		
		// create command
		CompareCmd cmd = new CompareCmd();

		// create the operation, add images and operators/options
		IMOperation op = new IMOperation();
		//op.compose("src");
		op.metric("ae");
		op.addImage();
		op.addImage();
		op.addImage("png:-");
		
		Stream2BufferedImage s2b = new Stream2BufferedImage();
		
		cmd.setOutputConsumer(s2b);
		
		try {
			cmd.run(op, initialScreen, currentScreen);
			comparisonResult.setPixelDifference("0"); // This code is only run if the images are identical - see comment about IM exit code handling
		} catch (IM4JavaException e) {
			// This catch is used for flow control, not error handling.
			// This is because IM is retarded and uses exit codes to display output
			// while IM4Java will (reasonably) pass any non-zero exit code as an exception.
			comparisonResult.setPixelDifference(
					e.toString() // will result in something like "org.im4java.core.CommandException: org.im4java.core.CommandException: 1607" where "1607" is the amount of pixels that are different
					.substring(70)
					);
		}
		
 		comparisonResult.setResultImage(s2b.getImage()); //TODO: If this is null, then the image compare failed, ie. the images are of different size or format
 		
 		return comparisonResult;
 	}
 	
}


