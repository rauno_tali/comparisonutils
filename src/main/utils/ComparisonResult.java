package main.utils;

import java.awt.image.BufferedImage;

public class ComparisonResult {
	private BufferedImage resultImage = null;
	private String pixelDifference = null;
	
	/**
	 * @return	BufferedImage which contains all of the pixels that are different highlighted in red
	 * 			This can be added to a report and viewed by a human to spot differences.
	 */
	public BufferedImage getResultImage() {
		return resultImage;
	}
	/**
	 * @return	String object which contains a the number of pixels which differ
	 *  		eg. "1603" (without quotes).
	 *  		This helps create automated asserts.
	 */
	public String getPixelDifference() {
		return pixelDifference;
	}
	public void setResultImage(BufferedImage resultImage) {
		this.resultImage = resultImage;
	}
	public void setPixelDifference(String pixelDifference) {
		this.pixelDifference = pixelDifference;
	}
	
}
