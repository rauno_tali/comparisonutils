<?xml version="1.0"?>
<!--
	File must be formed as follows
	- one root node
	 - at least one test node
	   each test node must have a name attribute which corresponds to a test name
	  - at least one data set node
	    all data set nodes within each test node must be of equal size
	   - at least one data item node
	     all data items must be in correct order
-->

<testsuite>

	<test name="Login">
		<testdata name="valid user">
			<dataitem name="email">automatedUser@hannas.ee</dataitem>
			<dataitem name="password">Parool123</dataitem>
		</testdata>
		<testdata name="invalid user">
			<dataitem name="email">invalidUser@hannas.ee</dataitem>
			<dataitem name="password">invalidParool123</dataitem>
		</testdata>
	</test>

	<test name="Create User">
		<testdata name="valid user">
			<dataitem name="email">automatedUser@hannas.ee</dataitem>
			<dataitem name="password">Parool123</dataitem>
			<dataitem name="Alert Text">User successfully created.</dataitem>
		</testdata>
	</test>

	<test name="Create Post">
		<testdata name="test post">
			<dataitem name="user">automatedUser@hannas.ee</dataitem>
			<dataitem name="password">Parool123</dataitem>
			<dataitem name="title">Test Title</dataitem>
			<dataitem name="content">Test Content</dataitem>
			<dataitem name="image filename">TestImage.jpg</dataitem>
		</testdata>
	</test>

	<test name="Delete Post">
		<testdata name="valid post">
			<dataitem name="user">automatedUser@hannas.ee</dataitem>
			<dataitem name="password">Parool123</dataitem>
			<dataitem name="title">Test Title</dataitem>
		</testdata>
	</test>

	<test name="Delete User">
		<testdata name="valid post">
			<dataitem name="user">automatedUser@hannas.ee</dataitem>
			<dataitem name="password">Parool123</dataitem>
			<dataitem name="title">Test Title</dataitem>
		</testdata>
	</test>

</testsuite>