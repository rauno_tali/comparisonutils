package main.utils;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import main.tests.TestBase;

public class CustomLogger extends TestListenerAdapter {
	
	//LOGGER & EXTENT TEST
	
	private static final Logger log = LogManager.getLogger(CustomLogger.class);
	protected static ExtentReports ext;
	private static ExtentTest test;
	
	public static void logInfo(String logMes)	{
		log.info(logMes);
		test.log(LogStatus.INFO, logMes);
	}
	
	public static void logError(String logMes)	{
		log.error(logMes);
	}
	
	public static void setUpExtentReport(){
		try {
			createReportFolder();
		} catch (Exception e) {
			logError("Unable to create testsuite report file setup.\n" + e.toString());
		}
	}
	
	public static void finishExtentReport(){
		ext.flush();
		ext.close();
	}
	
	//REPORTING UTILS
	
	public static String reportFolderName;
	
	public static void createReportFolder() throws Exception	{
		DateFormat dfFolder = new SimpleDateFormat("dd-MM-yy_HH-mm-ss");
		Date date = new Date();
		reportFolderName = "report_" + dfFolder.format(date);
		
		Path reportFolders = Paths.get(Constants.REPORTS_PATH.toString(), reportFolderName, Constants.SCREENSHOT_FOLDER_NAME);	
		File f = new File(reportFolders.toString());
		Path reportFolder = Paths.get(Constants.REPORTS_PATH.toString(), reportFolderName);
		File reportFile = new File(reportFolder.toString(), Constants.REPORT_FILE_NAME);
		
		if (!f.exists())	{
			f.mkdirs();
			reportFile.createNewFile();
			ext = new ExtentReports(reportFile.toString(), true);
			ext.config().documentTitle("Report " +  " " + dfFolder.format(date));
			ext.config().reportHeadline(dfFolder.format(date));
		}
	}
	
	//TODO: Take this method apart, does not represent functional paradigms used elsewhere very well
 	public static void takeScreenShot(String methodName, WebDriver driver) throws Exception{
		driver = TestBase.getDriver();
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	    DateFormat dateFormatfile = new SimpleDateFormat("dd-MM-yy_HH-mm-ss");
	    String resultFileName = "//screenshot_" + methodName + "_" + dateFormatfile.format(new Date()) + ".png";
	    Path toResultfile = Paths.get(Constants.REPORTS_PATH.toString(), reportFolderName, Constants.SCREENSHOT_FOLDER_NAME);
	    File resultFile = new File(toResultfile.toString(),  resultFileName);
	    FileUtils.copyFile(scrFile, resultFile);
	    CustomLogger.logInfo("Created screenshot " + resultFile + "\n");
	  	CustomLogger.getExtentTest().log(LogStatus.INFO, CustomLogger.getExtentTest().addScreenCapture(".//" + Constants.SCREENSHOT_FOLDER_NAME + resultFileName));
	}
 	
	public static String trimParameters(ITestResult tr)	{
 		return Arrays.toString(tr.getParameters()).replace("[", "(").replace("]", ")" );
 	}
 	
 	public static String convertStackTrace(Throwable trace){
 		StringWriter sw = new StringWriter();
 		PrintWriter pw = new PrintWriter(sw);
 		trace.printStackTrace(pw);     
 		return sw.toString();      
 	}
	
	//OVERRIDERS

	@Override
	public void onStart(ITestContext testContext){
		test = ext.startTest("");
	}
	
	@Override
	public void onTestStart(ITestResult tr)	{
		test = ext.startTest("Starting test " + tr.getName() + trimParameters(tr));
		log.info("STARTING TEST --- " + tr.getName() + trimParameters(tr));
	}
	
	@Override
	public void onTestSuccess(ITestResult tr)	{
		test.log(LogStatus.PASS, "TEST PASS");
		log.info("TEST PASS --- " + tr.getName() + trimParameters(tr) + "\n");
		ext.endTest(test);
	}

	@Override
	public void onTestSkipped(ITestResult tr)	{
		test.log(LogStatus.SKIP, "TEST SKIP");
		log.warn("TEST SKIP --- " + tr.getName() + trimParameters(tr) + "\n");
		ext.endTest(test);
	}
	
	@Override
	public void onTestFailure(ITestResult tr)	{
		test.log(LogStatus.FAIL, "TEST FAIL" + "<br>" + tr.getThrowable());
		log.error("TEST FAIL --- " + tr.getName() + trimParameters(tr) + "\n" + convertStackTrace(tr.getThrowable()));
//		String methodName = tr.getName().toString().trim();
//		try	{
//			takeScreenShot(methodName, TestBase.getDriver());
//		}
//		catch(Exception e)	{
//			log.error("Problem while taking screenshot\n" + e.toString());
//		}
		ext.endTest(test);
	}
	
	public static ExtentTest getExtentTest()	{
		return test;
	}
}
 