package main.runner;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;

import main.utils.ComparisonUtils;
import main.utils.Constants;

public class TestRunner {

	public static void main(String[] args) {
		ComparisonUtils.setImageMagickInstallPath("C:\\Program Files\\ImageMagick-6.9.3-Q16");
		List<String> file = new ArrayList<String>();
	    file.add(Constants.TESTDATA_PATH + "\\testng.xml");
	    TestNG testNG = new TestNG();
	    testNG.setUseDefaultListeners(false);
	    testNG.setTestSuites(file);
	    testNG.run();
	}

}