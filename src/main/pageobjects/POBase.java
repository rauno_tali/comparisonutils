package main.pageobjects;

import main.utils.CustomLogger;

public class POBase {
	
//	@FindBy (className = "alert")
//	private WebElement alert;
//	
//	public String getAlertText(){
//		return alert.getText();
//	}
	
	public static void logInfo(String logMes){
		CustomLogger.logInfo(logMes);
	}
	
	public static void logError(String logMes){
		CustomLogger.logError(logMes);
	}
	
}
