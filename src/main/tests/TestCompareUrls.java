package main.tests;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import main.utils.ComparisonUtils;
import main.utils.ComparisonResult;
import main.utils.Constants;
import main.utils.CustomLogger;

public class TestCompareUrls extends TestBase {

	private void writeImageToReport(String filename, BufferedImage image) throws IOException{
		DateFormat dateFormatfile = new SimpleDateFormat("dd-MM-yy_HH-mm-ss");
	    String resultFileName = "//screenshot_" + filename + "_" + dateFormatfile.format(new Date()) + ".png";
	    Path toResultfile = Paths.get(Constants.REPORTS_PATH.toString(), CustomLogger.reportFolderName, Constants.SCREENSHOT_FOLDER_NAME);
	    File resultFile = new File(toResultfile.toString(),  resultFileName);
	    
	    ImageIO.write(image, "png", resultFile);
	    
	  	CustomLogger.getExtentTest().log(LogStatus.INFO, CustomLogger.getExtentTest().addScreenCapture(".//" + Constants.SCREENSHOT_FOLDER_NAME + resultFileName));
	}
	
	@Test(dataProvider = "TestData")
	public void test(String filename, String url, String initializeString) throws Exception {
		
		driver.get(url);
		Thread.sleep(5000);
		
		if (initializeString.equals("Yes")) {
			ComparisonUtils.saveInitialScreenshot(Paths.get("testdata", "screenshots", filename).toString() + ".png", driver);
			logInfo("Saving new Expected sceenshot, then comparing.");
		}
		else if (initializeString.equals("No")) {
			logInfo("Comparing to previously saved image");
		}
		else {
			logError("Malformed \"Initialize\" attribute for test: quitting test, not initializing or comparing.");
			Assert.assertTrue(false, "\"Initialize\" attribute result \"" + initializeString + "\" is either \"Yes\" or \"No\"");
			return;
		}
		
		BufferedImage expectedScreen = ComparisonUtils.getInitialScreenshot(Paths.get("testdata", "screenshots", filename).toString() + ".png");
		BufferedImage actualScreen =
				ImageIO.read(
						((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE)
						);
		
//		BufferedImage comparisonImage =
//				CompareUtils.getComparisonImage(
//						expectedScreen,
//						actualScreen
//						);
		
		ComparisonResult comparisonResult =
				ComparisonUtils.getComparisonResult(expectedScreen, actualScreen);
		
		driver = TestBase.getDriver();
		
		writeImageToReport(filename + "_expected", expectedScreen);
		writeImageToReport(filename + "_actual", actualScreen);
		writeImageToReport(filename + "_comparison", comparisonResult.getResultImage());
		
		Assert.assertEquals(comparisonResult.getPixelDifference(), "0", "Comparison result's pixel difference");
		
//	    DateFormat dateFormatfile = new SimpleDateFormat("dd-MM-yy_HH-mm-ss");
//	    String resultFileName = "//screenshot_" + filename + "_" + dateFormatfile.format(new Date()) + ".png";
//	    Path toResultfile = Paths.get(Constants.REPORTS_PATH.toString(), CustomLogger.reportFolderName, Constants.SCREENSHOT_FOLDER_NAME);
//	    File resultFile = new File(toResultfile.toString(),  resultFileName);
//	    
//	    ImageIO.write(comparisonImage, "png", resultFile);
//	    
//	    CustomLogger.logInfo("Created screenshot " + resultFile + "\n");
//	  	CustomLogger.getExtentTest().log(LogStatus.INFO, CustomLogger.getExtentTest().addScreenCapture(".//" + Constants.SCREENSHOT_FOLDER_NAME + resultFileName));
		
		
		
		}

}
